import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/GeneralViews/Home'
import About from '@/components/GeneralViews/About'
import Register from '@/components/GeneralViews/Register'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },

    {
      path: '/sobre',
      name: 'About',
      component: About
    },

    {
      path: '/cadastro',
      name: 'Register',
      component: Register
    }
  ]
})
