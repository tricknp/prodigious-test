# Prodigious-Test

## Build Setup

``` bash
# instale as dependencias
npm install

# Para consumo de dados está sendo usado um banco de dados fake em um json server

# Inicie o Json Server
json-server --watch db.json

# Inicie o servidor
npm run dev
```
